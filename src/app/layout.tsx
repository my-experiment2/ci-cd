import type { Metadata } from 'next';
import { Inter } from 'next/font/google';

import './globals.css';

import { theme } from '../../mantine';

const inter = Inter({
  subsets: ['latin'],
  weight: ['300', '400', '500', '600', '700', '800'],
});

// Import styles of packages that you've installed.
// All packages except `@mantine/hooks` require styles imports
import { ColorSchemeScript, MantineProvider } from '@mantine/core';

import '@mantine/core/styles.css';

export const metadata: Metadata = {
  title: 'Paper Studio | Dashboard',
  description: 'Smart Ecosystem for Writing Paper',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <ColorSchemeScript />
      </head>
      <body className={inter.className}>
        <MantineProvider
          theme={{ ...theme, fontFamily: inter.style.fontFamily }}
        >
          {children}
        </MantineProvider>
      </body>
    </html>
  );
}
