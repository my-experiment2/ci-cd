import { Box, Button, Center, Group, Stack, Text, Title } from '@mantine/core';
import Image from 'next/image';

import Logo from '~/logo.jpg';

export default function Home() {
  return (
    <main>
      <Center py="xl" h="100vh">
        <Stack align="center" gap="sm">
          <Group gap="xs">
            <Image src={Logo} width={100} height={100} alt="Logo" />
            <Box>
              <Title>Paper Studio</Title>
              <Text>Smart Ecosystem for Writing Paper</Text>
            </Box>
          </Group>
          <Group gap="sm">
            <Button variant="default">Login</Button>
            <Button variant="filled">Register</Button>
          </Group>
        </Stack>
      </Center>
    </main>
  );
}
